<?php

namespace Drupal\entity_save;

use Drupal\file\Entity\File;

/**
 * Service for Deleting the entity.
 */
class EntitySaveHelper {

  /**
   * Get all content types from CMS.
   */
  public function getAllContentTypes() {
    $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();

    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    return $contentTypesList;
  }

  /**
   * Returns image fields based on content type.
   * @param type $content_type
   */
  public function getAllImageFields($bundle, $entity_type_id) {
    $bundleFields = [];
    foreach (\Drupal::entityManager()->getFieldDefinitions($entity_type_id, $bundle) as $field_name => $field_definition) {
      // Get only image type fields.
      if (!empty($field_definition->getTargetBundle()) && $field_definition->getType() == 'image') {
//        $bundleFields[$field_name]['type'] = $field_definition->getType();
        $bundleFields[$field_name] = $field_definition->getLabel();
      }
    }
    return $bundleFields;
  }

  /**
   * Write data in csv file to export functionality.
   * @param type $filename
   * Name of csv file.
   * @param type $content_types
   * Content type name.
   * @param type $image_fields
   * Image field name.
   * @return type
   * Return csv file url.
   */
  public function createCsvFileExportData($filename, $content_types, $image_fields, $export_body = NULL) {
    // Get file path in CMS.
    $path = drupal_realpath("public://$filename");
    $file = fopen($path, 'w');
    // send the column headers
    if (!empty($export_body)) {
      fputcsv($file, array('Nid', "Node_" . $content_types . '_Title', $image_fields, 'Alt', 'title', 'width', 'height', 'Summary', 'Description'));
    }
    else {
      fputcsv($file, array('Nid', "Node_" . $content_types . '_Title', $image_fields, 'Alt', 'title', 'width', 'height'));
    }
    // Sample data. This can be fetched from mysql too
    $nids = \Drupal::entityQuery('node')->condition('type', $content_types)->execute();
    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);
    foreach ($nodes as $node) {
      $row = [];
      $row[0] = $node->get('nid')->value;
      $row[1] = $node->get('title')->value;
      // Check image exits or not.
      if (!empty($node->get($image_fields)->target_id)) {
        $total_images = count($node->get($image_fields)->getValue());
        //Check multiple image value for this node.
        if ($total_images > 1) {
          $image_index = 0;
          foreach ($node->get($image_fields)->getValue() as $image_val) {
            $image_data = [];
            $image_data = File::load($image_val['target_id']);
            if (!empty($image_data)) {
              $basename[$image_index] = basename($image_data->getFileUri());
            }
            $alt[$image_index] = ($image_val['alt']) ? $image_val['alt'] : '';
            $img_title[$image_index] = ($image_val['title']) ? $image_val['title'] : '';
            $img_width[$image_index] = $image_val['width'];
            $img_height[$image_index] = $image_val['height'];
            $image_index++;
          }
          // Concate image values for multiple fields
          $row[2] = implode(" | ", $basename);
          $row[3] = implode(" | ", $alt);
          $row[4] = implode(" | ", $img_title);
          $row[5] = implode(" | ", $img_width);
          $row[6] = implode(" | ", $img_height);
        }
        else {
          $image_data = File::load($node->get($image_fields)->target_id);
          if (!empty($image_data)) {
            $row[2] = basename($image_data->getFileUri());
          }
          $row[3] = ($node->get($image_fields)->alt) ? $node->get($image_fields)->alt : '';
          $row[4] = ($node->get($image_fields)->title) ? $node->get($image_fields)->title : "";
          $row[5] = ($node->get($image_fields)->width) ? $node->get($image_fields)->width : "";
          $row[6] = ($node->get($image_fields)->height) ? $node->get($image_fields)->height : '';
        }
      }
      if (!empty($export_body)) {
        $row[7] = ($node->get('body')->summary) ? $node->get('body')->summary : '';
        $row[8] = ($node->get('body')->value) ? $node->get('body')->value : '';
      }
      // Write each record in csv file.
      fputcsv($file, $row);
    }
    fclose($file);
    return $path;
  }

  /**
   * Get orphan files from CMS.
   */
  public function deleteOrphanFiles() {
    // Create database connection.
    $query = \Drupal::database()->select('file_managed', 'fm')
      ->fields('fm', array('fid'))
      ->fields('fu', array('fid'));
    $query->addJoin('left', 'file_usage', 'fu', 'fm.fid=fu.fid');
    $query->condition('fu.fid', NULL, 'IS');
    $data = $query->execute();
    // Get all images from database.
    $results = $data->fetchAll(\PDO::FETCH_OBJ);
    foreach ($results as $row) {
      $image_data = File::load($row->fid);
      if (!empty($image_data)) {
        //Remove file from system.
        if (unlink(drupal_realpath($image_data->getFileUri()))) {
          // Remove Image file from database.
          \Drupal::database()->delete('file_managed')->condition('fid', $row->fid)->execute();
        }
      }
    }
  }

}
