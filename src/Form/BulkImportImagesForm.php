<?php

namespace Drupal\entity_save\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\entity_save\EntitySaveHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class EntitySaveForm.
 *
 * @package Drupal\entity_save\Form
 */
class BulkImportImagesForm extends FormBase {

  /**
   * A instance of the entity_save helper services.
   *
   * @var \Drupal\entity_save\EntitySaveHelper
   */
  protected $imageHelper;

  /**
   * A instance of the EntityTypeManagerInterface.
   *
   * @var \Drupal\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntitySaveHelper $imageHelper, EntityTypeManagerInterface $entityTypeManager) {
    $this->imageHelper = $imageHelper;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_save.batch_import_export'), $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bulk_image_export_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
// Choose action type.
    $form['import_type'] = [
      '#title' => t('Action Type'),
      '#type' => 'select',
      '#description' => 'Please choose your action like: export, import and delete unused files from CMS.',
      '#options' => ['export' => 'Export', 'import' => 'Import', 'delete' => 'Delete unused'],
    ];
// Get content types from CMS.

    $content_types = $this->imageHelper->getAllContentTypes();
    $selected = !empty($form_state->getValue('content_types')) ? $form_state->getValue('content_types') : key($content_types);
    $form['content_types'] = [
      '#title' => t('Content types'),
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => $selected,
      '#description' => 'Please select content type for import.',
      '#options' => $content_types,
      '#states' => [
        'invisible' => [':input[name="import_type"]' => ['value' => 'delete']],
      ],
      '#ajax' => [
        'callback' => '::ajax_example_dependent_dropdown_callback',
        'wrapper' => 'dropdown-second-replace',
      ],
    ];
// Get content types image fields.
    $form['image_fields'] = [
      '#title' => $content_types[$selected] . ' ' . t('Image field'),
      '#type' => 'select',
      '#options' => $this->imageHelper->getAllImageFields($selected, 'node'),
      '#default_value' => $selected,
      '#description' => 'Please select image field from  content type.',
      '#prefix' => '<div id="dropdown-second-replace">',
      '#suffix' => '</div>',
      '#states' => [
        'invisible' => [':input[name="import_type"]' => ['value' => 'delete']],
      ],
    ];

// Download sample files.
    $form['delete_confirm'] = [
      '#type' => 'checkbox',
      '#weight' => 11,
      '#description' => 'This action cannot be undone.',
      '#title' => t("Are you sure want to delete ?"),
      '#states' => [
        'visible' => [':input[name="import_type"]' => ['value' => 'delete']],
      ],
    ];

    //Export body summary.
    $form['export_body'] = [
      '#type' => 'checkbox',
      '#weight' => 11,
      '#description' => 'Include Body and summary as well (In case of import action Body, Summary will update).',
      '#title' => t("Body and Summary."),
      '#states' => [
        'invisible' => [':input[name="import_type"]' => ['value' => 'delete']],
      ],
    ];

    //Create node based on title.
    $form['new_node'] = [
      '#type' => 'checkbox',
      '#weight' => 12,
      '#description' => 'If nid not present in csv then create new node with title, summary,body and images fields.',
      '#title' => t("Create new node"),
      '#states' => [
        'visible' => [':input[name="import_type"]' => ['value' => 'import']],
      ],
    ];

// Select image file directory path.
//    $form['import_dir_path'] = [
//      '#type' => 'file',
//      '#weight' => 9,
//      '#required' => TRUE,
//      '#description' => 'Select image files directory, Where your files located.',
//      '#title' => t("Select image files directory"),
//      '#attributes' => ["multiple" => "", "directory" => "", "webkitdirectory" => "", "mozdirectory" => ""],
//      '#states' => [
//        'visible' => [':input[name="import_type"]' => ['value' => 'import']],
//      ],
//    ];
// Upload csv file.
    $form['importimage_csv'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Choose CSV File'),
      '#description' => t('Please upload csv file to create.'),
      '#upload_location' => 'public://imported-images/',
      '#weight' => 10,
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
    ];
// Import csv button.
    $form['import']['#type'] = 'actions';
    $form['import']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

// Attach library on form.
    $form['#attached']['library'][] = 'entity_save/import_images';
    return $form;
  }

  public function ajax_example_dependent_dropdown_callback(array &$form, FormStateInterface $form_state) {
    return $form['image_fields'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
// Delete confirmation validation.
    $delete_confirm = $form_state->getValue('delete_confirm');
    $import_type = $form_state->getValue('import_type');
    if (empty($delete_confirm) && $import_type == 'delete') {
      $form_state->setErrorByName('delete_confirm', t('Please check confirmation.'));
    }
// Import csv field validation.
    $importimage_csv = $form_state->getValue('importimage_csv');
    if (empty($importimage_csv) && $import_type == 'import') {
      $form_state->setErrorByName('importimage_csv', t('Please upload csv file.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Checked logged in admin user.
    $current = \Drupal::currentUser();
    $import_type = $form_state->getValue('import_type');
    $content_types = $form_state->getValue('content_types');
    $image_fields = $form_state->getValue('image_fields');
    $export_body = $form_state->getValue('export_body');
    switch ($import_type) {
      case "export":
        $filename = 'export_images_' . $current->id() . '.csv';
        $path = $this->imageHelper->createCsvFileExportData($filename, $content_types, $image_fields, $export_body);
        // Check file and export as csv format.
        if (is_file($path)) {
          $response = new BinaryFileResponse($path);
          $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename
          );
          $form_state->setResponse($response);
        }
        break;
      case "import":
        
        break;
      case "delete":
        $this->imageHelper->deleteOrphanFiles();
        drupal_set_message(t("Unused file deleted from CMS."));
        break;
    }
  }

}
